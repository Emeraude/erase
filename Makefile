SRC = src/main.c \
	src/args.c \
	src/erase.c \
	src/remove.c \
	src/verbose.c

OBJ = $(SRC:.c=.o)

NAME = erase

CFLAGS = -Wall -Wextra -std=gnu99

RM = rm -f

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
