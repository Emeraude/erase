#include "erase.h"

int main(int argc, char **argv) {
  settings = DEFAULT_SETTINGS;

  settings.av0 = argv[0];
  if (parse_args(argc, argv) == -1
      || erase_files() == -1)
    return 1;
  return 0;
}
