#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct {
  char **files;
  int file_count;
  int random_fd;
  int iter;
  int verbose;
  bool zero;
  bool remove;
  bool recursive;
  const char *av0;
} settings_t;

static const settings_t DEFAULT_SETTINGS = {
  .files = NULL,
  .file_count = 0,
  .random_fd = 0,
  .iter = 3,
  .verbose = 0,
  .zero = false,
  .remove = false,
  .recursive = false,
  .av0 = NULL
};

settings_t settings;

int parse_args(int argc, char **argv);
int erase_files();
int remove_regular_file(const char *file);
int remove_directory(const char *file);
void verbose(int level, const char *format, ...) __attribute__ ((format (printf, 2, 3)));
