#include <err.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "erase.h"

int parse_args(int argc, char **argv) {
  static struct option options[] = {
    {"iterations", required_argument, 0, 'n'},
    {"quiet", no_argument, 0, 'q'},
    {"random-source", required_argument, 0, 0},
    {"recursive", no_argument, 0, 'r'},
    {"remove", no_argument, 0, 'u'},
    {"verbose", no_argument, 0, 'v'},
    {"zero", no_argument, 0, 'z'},
    {0, 0, 0, 0}
  };
  int optindex = 0;
  int c;

  opterr = 1;
  while ((c = getopt_long(argc, argv, "n:qruvz", options, &optindex)) != -1) {
    if (!c) {
      if (!strcmp(options[optindex].name, "random-source")) {
	settings.random_fd = open(optarg, O_RDONLY);
	if (settings.random_fd == -1) {
	  warn("open");
	  return -1;
	}
      }
    }

    switch (c) {
    case 'n':
      settings.iter = atoi(optarg);
      break;
    case 'q':
      settings.verbose = 0;
      break;
    case 'r':
      settings.recursive = true;
      break;
    case 'u':
      settings.remove = true;
      break;
    case 'v':
      ++settings.verbose;
      break;
    case 'z':
      settings.zero = true;
      break;
    case '?':
      return -1;
    }
  }

  if (optind < argc) {
    settings.files = argv + optind;
    settings.file_count = argc - optind;
  } else {
    fprintf(stderr, "%s: missing file operand\n", argv[0]);
    return -1;
  }

  if (!settings.random_fd) {
    settings.random_fd = open("/dev/urandom", O_RDONLY);
    if (settings.random_fd == -1) {
      warn("open");
      return -1;
    }
  }

  return 0;
}
