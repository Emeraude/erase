#include <err.h>
#include <libgen.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "erase.h"

enum file_type {
  REGULAR, DIRECTORY
};

int truncate_file_name(char *oldpath, enum file_type file_type) {
  char *newpath = strdup(oldpath);

  if (newpath == NULL) {
    warn("strdup");
    return -1;
  }
  const unsigned int len = strlen(basename(oldpath));
  for (unsigned int i = 1; i < len; ++i) {
    char *file = basename(newpath);
    file[len - i] = '\0';
    if (rename(oldpath, newpath) == -1) {
      warn("rename");
      return -1;
    }
    verbose(2, "%s: renamed to %s\n", oldpath, newpath);
    strcpy(oldpath, newpath);
  }
  if (file_type == REGULAR) {
    if (unlink(newpath) == -1) {
      warn("unlink");
      return -1;
    }
  }
  else {
    if (rmdir(newpath) == -1) {
      warn("rmdir");
      return -1;
    }
  }
  free(newpath);
  return 0;
}

int remove_file(const char *path, enum file_type file_type) {
  char *edit_path = strdup(path);
  char *file;

  if (edit_path == NULL) {
    warn("strdup");
    return -1;
  }
  file = basename(edit_path);
  sprintf(file, "%0*d", (int)strlen(file), 0);
  if (rename(path, edit_path) == -1) {
    warn("rename");
    return -1;
  }
  verbose(1, "%s: renamed to %s\n", path, edit_path);
  truncate_file_name(edit_path, file_type);
  verbose(1, "%s: removed\n", path);
  free(edit_path);
  return 0;
}

int remove_regular_file(const char *path) {
  return remove_file(path, REGULAR);
}

int remove_directory(const char *path) {
  return remove_file(path, DIRECTORY);
}
