#define _GNU_SOURCE
# include <stdio.h>
#undef _GNU_SOURCE
#include <dirent.h>
#include <err.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "erase.h"

int erase_file(const char *file);

int erase_dir(const char *file) {
  DIR *dirp = opendir(file);
  struct dirent *dir_entry;

  if (dirp == NULL) {
    warn("opendir");
    return -1;
  }
  while ((dir_entry = readdir(dirp))) {
    if (strcmp(dir_entry->d_name, ".")
	&& strcmp(dir_entry->d_name, "..")) {
      char *full_path;
      asprintf(&full_path, "%s/%s", file, dir_entry->d_name);
      if (full_path == NULL) {
	warn("asprintf");
	return -1;
      }
      erase_file(full_path);
      free(full_path);
    }
  }

  /* TODO: check readdir and closedir return values? */
  closedir(dirp);

  if (settings.remove) {
    if (remove_directory(file) == -1)
      return -1;
  }
  return 0;
}

int write_random(off_t st_size, int file_fd, int random_fd) {
  off_t o = 0;
  char buffer[4096];

  if (lseek(file_fd, 0, SEEK_SET) == -1) {
    warn("lseek");
    return -1;
  }
  while (o < st_size) {
    int readed;
    if ((readed = read(random_fd, buffer, sizeof(buffer))) == -1) {
      warn("read");
      return -1;
    }
    else if (readed != sizeof(buffer)) {
      fprintf(stderr, "%s: random source: end of file\n", settings.av0);
      return -1;
    }
    int written = write(file_fd, buffer, sizeof(buffer));
    if (written == -1) {
      warn("write");
      return -1;
    }
    o += written;
  }
  return 0;
}

int erase_file_content(const char *file) {
  struct stat sb;
  int fd = open(file, O_WRONLY);

  if (fd == -1) {
    warn("open");
    return -1;
  }
  else if (stat(file, &sb) == -1){
    warn("stat");
    return -1;
  }

  for (int i = 0; i < settings.iter; ++i) {
    verbose(1, "%s: pass %i/%i (random)...\n", file, i + 1, settings.iter + (settings.zero ? 1 : 0));
    if (write_random(sb.st_size, fd, settings.random_fd) == -1)
      return -1;
  }
  if (settings.zero) {
    int zfd = open("/dev/zero", O_RDONLY);

    if (zfd == -1) {
      warn("open");
      return -1;
    }
    verbose(1, "%s: pass %i/%i (000000)...\n", file, settings.iter + (settings.zero ? 1 : 0), settings.iter + 1);
    if (write_random(sb.st_size, fd, zfd) == -1)
      return -1;
  }
  if (close(fd) == -1) {
    warn("close");
    return -1;
  }
  if (settings.remove) {
    if (remove_regular_file(file) == -1)
      return -1;
  }
  return 0;
}

int erase_file(const char *file) {
  struct stat sb;

  if (stat(file, &sb) == -1) {
    warn("stat");
    return -1;
  }
  if ((sb.st_mode & S_IFMT) == S_IFDIR
      && settings.recursive)
    erase_dir(file);
  else {
    if (erase_file_content(file) == -1)
      return -1;
  }
  return 0;
}

int erase_files() {
  for (int i = 0; i < settings.file_count; ++i) {
    if (erase_file(settings.files[i]) == -1)
      return -1;
  }
  return 0;
}
