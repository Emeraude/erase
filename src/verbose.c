#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "erase.h"

void verbose(int level, const char *format, ...) {
  va_list args;
  va_start(args, format);

  if (settings.verbose >= level) {
    printf("%s: ", settings.av0);
    vprintf(format, args);
  }
  va_end(args);
}
