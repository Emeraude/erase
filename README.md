# Erase - A file eraser tool

This project is an improved version of the `shred` command.

## Features

Feature | Shred | Erase
---|:---:|:---:
File shredding | Yes | Yes
-f option | Yes | Not yet planned
-x option | Yes | Not yet planned
-s option | Yes | Not yet planned
Display progress | Yes | Planned
Random source option | Yes | Yes
Zero-filling option (-z) | Yes | Yes
Iterations count option (-n) | Yes | Yes
File removing option (-u) | Yes | Yes
Verbose option\* | Partial | Partial (third level planned)
Prompt on files with several hard links | No | Planned (via an argument)
Interactive option (-i) | No | Planned
Iterations setting\*\* | No | Planned
Recursive option | No | Yes
Directory name erasing (on option -u) | No | Yes

\* There are several levels of verbose in `erase`  
\*\* There are two ways of deleting several files several times in `erase` : file by file or pass by pass
